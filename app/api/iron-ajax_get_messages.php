<?php

require_once "access_controll.php";	// Allow CORS

session_start();

header ("Content-type: application/json");

if (isset($_SESSION['polymer_demo_data'])) {
	echo json_encode($_SESSION['polymer_demo_data']);
} else {
	echo "[]";	// Empty json array :-)
}