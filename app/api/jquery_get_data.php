<?php

require_once "access_controll.php";	// Allow CORS

session_start();

header ("Content-type: application/json");

$name = array ("given"=>"Øivind", "family"=>"Kolloen");
$imt1401 = array ("code"=>"IMT1401", "name"=>"Informasjons- og publiseringsteknologi");
$imt2291 = array ("code"=>"IMT2291", "name"=>"WWW teknologi");
$imt3281 = array ("code"=>"IMT3281", "name"=>"Applikasjonsutvikling");
$subjects = array ($imt1401, $imt2291, $imt3281);

echo json_encode (array("name"=>$name, "subjects"=>$subjects, "session"=>$_SESSION));