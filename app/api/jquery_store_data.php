<?php

require_once "access_controll.php";	// Allow CORS

session_start();

header ("Content-type: application/json");

if (isset($_SESSION['polymer_demo_data'])) {
	// Could have just pushed $_POST onto the session array element.
	// This is just to make it clear what happens
	array_push ($_SESSION['polymer_demo_data'], array ("name"=>$_POST["name"], "message"=>$_POST["message"]));
} else {
	$_SESSION['polymer_demo_data'][] = array ("name"=>$_POST["name"], "message"=>$_POST["message"]);
}

echo json_encode (array ("status"=>"OK", "message"=>"Bruk knappen 'Gjør AJAX GET request' og se på session objektet for å se hva som er lagret", "Tip"=>"For debugging kan det være greit å sende med innholdet av variable tilbake til scriptet","post"=>$_POST));