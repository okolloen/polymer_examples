<?php

require_once "access_controll.php";	// Allow CORS

session_start();

header ("Content-type: application/json");

// Data from iron-ajax does not get send by regular POST form data format
// It must be read from the php://input
$req = file_get_contents('php://input');
// Then json_decoded, the "true" means put it into an assosiative array
$data = json_decode($req, true);

if (isset($_SESSION['polymer_demo_data'])) {
	// Could have just pushed $_POST onto the session array element.
	// This is just to make it clear what happens
	array_push ($_SESSION['polymer_demo_data'], array ("name"=>$data["name"], "message"=>$data["message"]));
} else {
	$_SESSION['polymer_demo_data'][] = array ("name"=>$data["name"], "message"=>$data["message"]);
}

echo json_encode (array ("status"=>"OK", "message"=>"Trykk på menyvalget 'iron-ajax' for å se oppdatert liste over meldinger", "Tip"=>"For debugging kan det være greit å sende med innholdet av variable tilbake til scriptet","req"=>$req, "data"=>$data));